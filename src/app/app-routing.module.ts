import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    // redirectTo: 'transaksi',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'transaksi',
    loadChildren: () => import('./pages/transaksi/transaksi.module').then(m => m.TransaksiPageModule)
  },
  {
    path: 'transaksi-pelanggan',
    loadChildren: () => import('./pages/transaksi/transaksi-pelanggan/transaksi-pelanggan.module').then( m => m.TransaksiPelangganPageModule)
  },
  {
    path: 'transaksi-detail/:id/:nota/:waktu/:id_pelanggan/:nama_pelanggan/:harga_ro/:kuantiti_ro/:harga_biasa/:kuantiti_biasa/:catatan',
    loadChildren: () => import('./pages/transaksi/transaksi-detail/transaksi-detail.module').then(m => m.TransaksiDetailPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'produk',
    loadChildren: () => import('./pages/produk/produk.module').then(m => m.ProdukPageModule)
  },
  {
    path: 'produk-detail/:id/:nama_produk/:harga/:keterangan',
    loadChildren: () => import('./pages/produk/produk-detail/produk-detail.module').then(m => m.ProdukDetailPageModule)
  },
  {
    path: 'pelanggan',
    loadChildren: () => import('./pages/pelanggan/pelanggan.module').then(m => m.PelangganPageModule)
  },
  {
    path: 'pelanggan-detail/:id/:nama_pelanggan/:alamat/:catatan',
    loadChildren: () => import('./pages/pelanggan/pelanggan-detail/pelanggan-detail.module').then(m => m.PelangganDetailPageModule)
  },
  {
    path: 'pengeluaran',
    loadChildren: () => import('./pages/pengeluaran/pengeluaran.module').then(m => m.PengeluaranPageModule)
  },
  {
    path: 'pengeluaran-detail/:id/:waktu/:nama_pengeluaran/:nominal',
    loadChildren: () => import('./pages/pengeluaran/pengeluaran-detail/pengeluaran-detail.module').then(m => m.PengeluaranDetailPageModule)
  },
  {
    path: 'laporan',
    loadChildren: () => import('./pages/laporan/laporan.module').then( m => m.LaporanPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
