import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  password: string = 'bismillah';

  constructor(
    public appComponent: AppComponent,
    public menu: MenuController,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  login() {
    if (!this.password) {
      alert("silahkan isi password.")
    } else if (this.password == 'bismillah') {
      this.menu.enable(true);
      this.navCtrl.navigateRoot('/transaksi').then(() => {
        this.appComponent.pubLogin({});
      });

    } else {
      alert("ups, password salah.");
    }
  }

  ionViewWillLeave() {
    this.appComponent.pubLogin({});
  }

  ionViewDidLeave() {
    this.appComponent.pubLogin({});
  }

}
