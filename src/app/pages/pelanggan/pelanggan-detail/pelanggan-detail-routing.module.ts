import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PelangganDetailPage } from './pelanggan-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PelangganDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PelangganDetailPageRoutingModule {}
