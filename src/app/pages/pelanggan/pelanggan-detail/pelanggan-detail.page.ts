import { AppComponent } from '../../../app.component';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Location } from "@angular/common";

@Component({
  selector: 'app-pelanggan-detail',
  templateUrl: './pelanggan-detail.page.html',
  styleUrls: ['./pelanggan-detail.page.scss'],
})
export class PelangganDetailPage implements OnInit {

  id: number;
  nama_pelanggan: string;
  alamat: string;
  catatan: string;

  constructor(
    public appComponent: AppComponent,
    public http: HttpClient,
    public location: Location,
    public route: ActivatedRoute,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.nama_pelanggan = this.route.snapshot.paramMap.get('nama_pelanggan');

    if (this.route.snapshot.paramMap.get('alamat') != 'null') {
      this.alamat = this.route.snapshot.paramMap.get('alamat');
    }

    if (this.route.snapshot.paramMap.get('catatan') != 'null') {
      this.catatan = this.route.snapshot.paramMap.get('catatan');
    }
  }

  tambah() {
    if (!this.nama_pelanggan) {
      alert('Silahkan isi Nama Pelanggan');
    } else {
      let url = 'https://ubmart.online/api_sab/pelanggan_create.php';
      let body = new FormData();
      body.append('nama_pelanggan', this.nama_pelanggan);
      body.append('alamat', this.alamat.toString());
      body.append('catatan', this.catatan);

      this.http.post(url, body).subscribe(async (data: any) => {
        if (data.status == true) {

          // this.appComponent.pubPelanggan({});
          this.location.back();

          const toast = await this.toastController.create({
            message: 'Berhasil menambah data baru.',
            position: 'top',
            color: 'success',
            duration: 2000,
            mode: 'md',
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
        } else {
          const toast = await this.toastController.create({
            message: 'Gagal menambah data baru.\n' + data.message,
            position: 'top',
            color: 'warning',
            duration: 2000,
            mode: 'md',
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
        }
      }, error => {
        console.info(error.error);
      });
    }
  }

  update() {
    if (!this.nama_pelanggan) {
      alert('Silahkan isi Nama pelanggan.');
    } else {
      let url = 'https://ubmart.online/api_sab/pelanggan_update.php';
      let body = new FormData();
      body.append('id', this.id.toString());
      body.append('nama_pelanggan', this.nama_pelanggan);
      body.append('alamat', this.alamat.toString());
      body.append('catatan', this.catatan);
      body.append('status', '1');

      this.http.post(url, body).subscribe(async (data: any) => {
        if (data.status == true) {

          // this.appComponent.pubPelanggan({});
          this.location.back();

          const toast = await this.toastController.create({
            message: 'Berhasil mengupdate data.',
            position: 'top',
            color: 'success',
            duration: 2000,
            mode: 'md',
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
        } else {
          const toast = await this.toastController.create({
            message: 'Gagal mengubah data.\n' + data.message,
            position: 'top',
            color: 'warning',
            duration: 2000,
            mode: 'md',
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
        }
      }, error => {
        console.info(error.error);
      });
    }
  }

  hapus() {
    let url = 'https://ubmart.online/api_sab/pelanggan_update.php';
    let body = new FormData();
    body.append('id', this.id.toString());
    body.append('nama_pelanggan', this.nama_pelanggan);
    body.append('alamat', this.alamat.toString());
    body.append('catatan', this.catatan);
    body.append('status', '0');

    this.http.post(url, body).subscribe(async (data) => {
      console.info(data);

      // this.appComponent.pubPelanggan({});
      this.location.back();

      const toast = await this.toastController.create({
        message: 'Berhasil menghapus data.',
        position: 'top',
        color: 'dark',
        duration: 2000,
        mode: 'md',
        buttons: [
          {
            text: 'X',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
    }, error => {
      console.info(error.error);
    });
  }
}