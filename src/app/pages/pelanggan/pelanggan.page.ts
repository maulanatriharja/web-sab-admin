import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.page.html',
  styleUrls: ['./pelanggan.page.scss'],
})
export class PelangganPage implements OnInit {

  data: any = [];
  loading = true;

  constructor(
    // public appComponent: AppComponent,
    public http: HttpClient,
  ) { }

  ngOnInit() {
  }
  
  ionViewDidEnter() {
    // this.appComponent.obvProduk().subscribe(() => {
    //   this.load_data('');
    // });

    this.load_data('');
  }

  load_data(event: any) {
    let url = 'https://ubmart.online/api_sab/pelanggan_read.php?nama_pelanggan=' + event;

    this.http.get(url).subscribe((data) => {
      console.info(data);

      this.data = data;

      this.loading = false;
    }, error => {
      console.info(error.error);

      this.loading = false;
    });
  }
}