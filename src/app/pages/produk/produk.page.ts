import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-produk',
  templateUrl: './produk.page.html',
  styleUrls: ['./produk.page.scss'],
})
export class ProdukPage implements OnInit {

  data: any = [];
  loading = true;

  constructor(
    public appComponent: AppComponent,
    public http: HttpClient,
    public router: Router,
  ) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    // this.appComponent.obvProduk().subscribe(() => {
    //   this.load_data();
    // });

    this.load_data();
  }

  load_data() {
    let url = 'https://ubmart.online/api_sab/produk_read.php';

    this.http.get(url).subscribe((data) => {
      console.info(data);

      this.data = data;

      this.loading = false;
    }, error => {
      console.info(error.error);

      this.loading = false;
    });
  }
}