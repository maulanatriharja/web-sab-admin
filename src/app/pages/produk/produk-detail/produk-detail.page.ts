import { AppComponent } from '../../../app.component';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Location } from "@angular/common";

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.page.html',
  styleUrls: ['./produk-detail.page.scss'],
})
export class ProdukDetailPage implements OnInit {

  id: number;
  nama_produk: string;
  harga: number;
  keterangan: string = '';

  constructor(
    public appComponent: AppComponent,
    public http: HttpClient,
    public location: Location,
    public route: ActivatedRoute,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.nama_produk = this.route.snapshot.paramMap.get('nama_produk');
    this.harga = parseInt(this.route.snapshot.paramMap.get('harga'));

    if (this.route.snapshot.paramMap.get('keterangan') != 'null') {
      this.keterangan = this.route.snapshot.paramMap.get('keterangan');
    }
  }

  update() {
    if (!this.nama_produk) {
      alert('Silahkan isi Nama Produk.');
    } else if (!this.harga) {
      alert('Silahkan isi Harga.');
    } else {
      let url = 'https://ubmart.online/api_sab/produk_update.php';
      let body = new FormData();
      body.append('id', this.id.toString());
      body.append('nama_produk', this.nama_produk);
      body.append('harga', this.harga.toString());
      body.append('keterangan', this.keterangan);

      this.http.post(url, body).subscribe(async (data) => {
        console.info(data);

        // this.appComponent.pubProduk({});
        // this.navCtrl.navigateRoot('/tabs/produk');
        this.location.back();

        const toast = await this.toastController.create({
          message: 'Berhasil mengupdate data.',
          position: 'top',
          color: 'success',
          duration: 2000,
          mode:'md',
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();
      }, error => {
        console.info(error.error);
      });
    }
  }
}