import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';

import * as moment from 'moment';
import { not } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-transaksi-detail',
  templateUrl: './transaksi-detail.page.html',
  styleUrls: ['./transaksi-detail.page.scss'],
})
export class TransaksiDetailPage implements OnInit {

  data_produk: any = [];

  id: string;
  nota: string;
  waktu: string;
  id_pelanggan: string;
  nama_pelanggan: string;
  harga_ro: any;
  kuantiti_ro: any;
  harga_biasa: any;
  kuantiti_biasa: any;
  catatan: string;

  constructor(
    public http: HttpClient,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (this.route.snapshot.paramMap.get('nota')) {
      this.nota = this.route.snapshot.paramMap.get('nota');
    } else {
      this.nota = moment().format('YYMMDDHHmmss');
    }

    if (this.route.snapshot.paramMap.get('waktu')) {
      this.waktu = this.route.snapshot.paramMap.get('waktu').substring(0, 10);
    } else {
      this.waktu = moment().format('YYYY-MM-DD');
    }

    this.id = this.route.snapshot.paramMap.get('id');
    this.id_pelanggan = this.route.snapshot.paramMap.get('id_pelanggan');
    this.nama_pelanggan = this.route.snapshot.paramMap.get('nama_pelanggan');

    if (this.route.snapshot.paramMap.get('harga_ro') != 'null') {
      this.harga_ro = this.route.snapshot.paramMap.get('harga_ro');
    } else {
      this.harga_ro = '';
    }

    if (this.route.snapshot.paramMap.get('kuantiti_ro') != 'null') {
      this.kuantiti_ro = this.route.snapshot.paramMap.get('kuantiti_ro');
    } else {
      this.kuantiti_ro = '';
    }

    if (this.route.snapshot.paramMap.get('harga_biasa') != 'null') {
      this.harga_biasa = this.route.snapshot.paramMap.get('harga_biasa');
    } else {
      this.harga_biasa = '';
    }

    if (this.route.snapshot.paramMap.get('kuantiti_biasa') != 'null') {
      this.kuantiti_biasa = this.route.snapshot.paramMap.get('kuantiti_biasa');
    } else {
      this.kuantiti_biasa = '';
    }

    this.catatan = this.route.snapshot.paramMap.get('catatan');

    // if (!this.kuantiti_ro) { this.kuantiti_ro = 0; }
    // if (!this.kuantiti_biasa) { this.kuantiti_biasa = 0; }

    this.load_produk();
  }

  load_produk() {
    let url = 'https://ubmart.online/api_sab/produk_read.php';

    this.http.get(url).subscribe((data) => {
      console.info(data);
      this.data_produk = data;

      if (!this.harga_ro) { this.harga_ro = this.data_produk[0].harga; }
      if (!this.harga_biasa) { this.harga_biasa = this.data_produk[1].harga; }
    }, error => {
      console.info(error.error);
    });
  }

  tambah() {
    let url = 'https://ubmart.online/api_sab/transaksi_create.php';
    let body = new FormData();
    body.append('nota', this.nota);
    body.append('waktu', this.waktu);
    body.append('id_pelanggan', this.id_pelanggan);
    body.append('catatan', this.catatan);

    body.append('harga_ro', this.harga_ro);
    body.append('kuantiti_ro', this.kuantiti_ro);
    body.append('harga_biasa', this.harga_biasa);
    body.append('kuantiti_biasa', this.kuantiti_biasa);

    this.http.post(url, body).subscribe(async (data) => {
      console.info(data);

      this.navCtrl.navigateRoot('/transaksi');

      const toast = await this.toastController.create({
        message: 'Berhasil menambah data baru.',
        position: 'top',
        color: 'success',
        duration: 2000,
        mode: 'md',
        buttons: [
          {
            text: 'X',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
    }, error => {
      console.info(error.error);
    });
  }

  update() {
    let url = 'https://ubmart.online/api_sab/transaksi_update.php';
    let body = new FormData();
    body.append('nota', this.nota);
    body.append('waktu', this.waktu);
    body.append('catatan', this.catatan);

    body.append('harga_ro', this.harga_ro);
    body.append('kuantiti_ro', this.kuantiti_ro);
    body.append('harga_biasa', this.harga_biasa);
    body.append('kuantiti_biasa', this.kuantiti_biasa);

    this.http.post(url, body).subscribe(async (data) => {
      console.info(data);

      this.navCtrl.navigateRoot('/transaksi');

      const toast = await this.toastController.create({
        message: 'Berhasil mengupdate data.',
        position: 'top',
        color: 'success',
        duration: 2000,
        mode: 'md',
        buttons: [
          {
            text: 'X',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
    }, error => {
      console.info(error.error);
    });
  }

  hapus() {
    let url = 'https://ubmart.online/api_sab/transaksi_delete.php';
    let body = new FormData();
    body.append('id', this.id.toString());

    this.http.post(url, body).subscribe(async (data) => {
      console.info(data);

      this.navCtrl.navigateRoot('/transaksi');

      const toast = await this.toastController.create({
        message: 'Berhasil menghapus data.',
        position: 'top',
        color: 'dark',
        duration: 2000,
        mode: 'md',
        buttons: [
          {
            text: 'X',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
    }, error => {
      console.info(error.error);
    });
  }

}
