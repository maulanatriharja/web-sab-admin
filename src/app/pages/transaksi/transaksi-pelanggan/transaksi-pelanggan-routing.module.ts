import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransaksiPelangganPage } from './transaksi-pelanggan.page';

const routes: Routes = [
  {
    path: '',
    component: TransaksiPelangganPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransaksiPelangganPageRoutingModule {}
