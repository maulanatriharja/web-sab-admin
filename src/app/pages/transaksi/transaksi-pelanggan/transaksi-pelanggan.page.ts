import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-transaksi-pelanggan',
  templateUrl: './transaksi-pelanggan.page.html',
  styleUrls: ['./transaksi-pelanggan.page.scss'],
})
export class TransaksiPelangganPage implements OnInit {

  data: any = [];
  loading = false;

  constructor(
    public http: HttpClient,
  ) { }

  ngOnInit() {
    this.load_data('');
  }

  load_data(event: string) {
    if (event.length > 0) {
      this.loading=true;
      
      let url = 'https://ubmart.online/api_sab/pelanggan_read.php?nama_pelanggan=' + event;

      this.http.get(url).subscribe((data) => {
        console.info(data);

        this.data = data;
        this.loading = false;
      }, error => {
        console.info(error.error);
        this.loading = false;
      });
    } else {
      this.data = [];
      this.loading = false;
    }
  }
}
