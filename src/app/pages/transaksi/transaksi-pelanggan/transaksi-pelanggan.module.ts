import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransaksiPelangganPageRoutingModule } from './transaksi-pelanggan-routing.module';

import { TransaksiPelangganPage } from './transaksi-pelanggan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransaksiPelangganPageRoutingModule
  ],
  declarations: [TransaksiPelangganPage]
})
export class TransaksiPelangganPageModule {}
