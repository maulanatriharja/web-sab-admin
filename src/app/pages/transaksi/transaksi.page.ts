import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from '../../app.component';

import * as moment from 'moment';

@Component({
  selector: 'app-transaksi',
  templateUrl: './transaksi.page.html',
  styleUrls: ['./transaksi.page.scss'],
})
export class TransaksiPage implements OnInit {

  bulan: any;
  tahun: any;

  data: any = [];
  loading = true;

  total_h_ro: number = 0;
  total_q_ro: number = 0;
  total_h_bi: number = 0;
  total_q_bi: number = 0;
  total: number = 0;

  constructor(
    public appComponent: AppComponent,
    public http: HttpClient,
  ) { }

  ngOnInit() {
    // this.appComponent.pubLogin({});
    this.bulan = moment().format('M');
    this.tahun = moment().format('YYYY');
  }

  ionViewDidEnter() {
    this.load_data('');
  }

  load_data(event: any) {
    let url = 'https://ubmart.online/api_sab/transaksi_read.php?tahun=' + this.tahun + '&bulan=' + this.bulan + '&nama_pelanggan=' + event;

    this.http.get(url).subscribe((data: any) => {
      console.info(data);

      this.data = [];

      this.total_h_ro = 0;
      this.total_q_ro = 0;
      this.total_h_bi = 0;
      this.total_q_bi = 0;
      this.total = 0;

      for (let i = 0; i < data.length; i++) {
        this.data.push(data[i]);

        if (parseInt(data[i].harga_ro) > 0) { this.total_h_ro += parseInt(data[i].harga_ro) };
        if (parseInt(data[i].kuantiti_ro) > 0) { this.total_q_ro += parseInt(data[i].kuantiti_ro) };
        if (parseInt(data[i].harga_biasa) > 0) { this.total_h_bi += parseInt(data[i].harga_biasa) };
        if (parseInt(data[i].kuantiti_biasa) > 0) { this.total_q_bi += parseInt(data[i].kuantiti_biasa) };

        this.total += parseInt(data[i].total);
      }

      this.loading = false;
    }, error => {
      console.info(error.error);

      this.loading = false;
    });
  }
}
