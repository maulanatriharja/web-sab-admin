import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as moment from 'moment';

@Component({
  selector: 'app-pengeluaran',
  templateUrl: './pengeluaran.page.html',
  styleUrls: ['./pengeluaran.page.scss'],
})
export class PengeluaranPage implements OnInit {

  bulan: any;
  tahun: any;

  data: any = [];
  loading = true;

  total: number = 0;

  constructor(
    public http: HttpClient,
  ) { }

  ngOnInit() {
    this.bulan = moment().format('M');
    this.tahun = moment().format('YYYY');
  }

  ionViewDidEnter() {
    this.load_data('');
  }

  load_data(event: any) {


    let url = 'https://ubmart.online/api_sab/pengeluaran_read.php?tahun=' + this.tahun + '&bulan=' + this.bulan + '&nama_pengeluaran=' + event;

    this.http.get(url).subscribe((data: any) => {
      console.info(data);
      this.data = [];
      this.total = 0;

      for (let i = 0; i < data.length; i++) {
        this.data.push(data[i]);
        this.total += parseInt(data[i].nominal);
      }

      this.loading = false;
    }, error => {
      console.info(error.error);

      this.loading = false;
    });
  }
}