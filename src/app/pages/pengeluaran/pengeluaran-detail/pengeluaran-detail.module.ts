import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PengeluaranDetailPageRoutingModule } from './pengeluaran-detail-routing.module';

import { PengeluaranDetailPage } from './pengeluaran-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PengeluaranDetailPageRoutingModule,
  ],
  declarations: [PengeluaranDetailPage]
})
export class PengeluaranDetailPageModule { }
