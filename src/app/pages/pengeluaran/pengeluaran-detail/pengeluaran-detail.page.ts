import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Location } from "@angular/common";

import * as moment from 'moment';

@Component({
  selector: 'app-pengeluaran-detail',
  templateUrl: './pengeluaran-detail.page.html',
  styleUrls: ['./pengeluaran-detail.page.scss'],
})
export class PengeluaranDetailPage implements OnInit {

  id: number;
  waktu: string;
  nama_pengeluaran: string;
  nominal: string;

  constructor(
    public http: HttpClient,
    public location: Location,
    public route: ActivatedRoute,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.waktu = this.route.snapshot.paramMap.get('waktu');
    this.nama_pengeluaran = this.route.snapshot.paramMap.get('nama_pengeluaran');
    this.nominal = this.route.snapshot.paramMap.get('nominal');
  }

  tambah() {
    if (!this.nama_pengeluaran) {
      alert('Silahkan isi Nama Pelanggan');
    } else {
      let url = 'https://ubmart.online/api_sab/pengeluaran_create.php';
      let body = new FormData();
      body.append('waktu', this.waktu);
      body.append('nama_pengeluaran', this.nama_pengeluaran);
      body.append('nominal', this.nominal);

      this.http.post(url, body).subscribe(async (data) => {
        console.info(data);

        // this.appComponent.pubPelanggan({});
        this.location.back();

        const toast = await this.toastController.create({
          message: 'Berhasil menambah data baru.',
          position: 'top',
          color: 'success',
          duration: 2000,
          mode: 'md',
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();
      }, error => {
        console.info(error.error);
      });
    }
  }

  update() {
    if (!this.nama_pengeluaran) {
      alert('Silahkan isi Nama pengeluaran.');
    } else {
      let url = 'https://ubmart.online/api_sab/pengeluaran_update.php';
      let body = new FormData();
      body.append('id', this.id.toString());
      body.append('waktu', this.waktu);
      body.append('nama_pengeluaran', this.nama_pengeluaran);
      body.append('nominal', this.nominal);

      this.http.post(url, body).subscribe(async (data) => {
        console.info(data);

        // this.appComponent.pubPelanggan({});
        this.location.back();

        const toast = await this.toastController.create({
          message: 'Berhasil mengupdate data.',
          position: 'top',
          color: 'success',
          duration: 2000,
          mode: 'md',
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();
      }, error => {
        console.info(error.error);
      });
    }
  }

  hapus() {
    let url = 'https://ubmart.online/api_sab/pengeluaran_delete.php';
    let body = new FormData();
    body.append('id', this.id.toString());

    this.http.post(url, body).subscribe(async (data) => {
      console.info(data);

      // this.appComponent.pubPelanggan({});
      this.location.back();

      const toast = await this.toastController.create({
        message: 'Berhasil menghapus data.',
        position: 'top',
        color: 'dark',
        duration: 2000,
        mode: 'md',
        buttons: [
          {
            text: 'X',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
    }, error => {
      console.info(error.error);
    });
  }
}