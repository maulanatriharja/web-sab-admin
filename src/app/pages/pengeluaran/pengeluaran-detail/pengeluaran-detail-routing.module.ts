import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PengeluaranDetailPage } from './pengeluaran-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PengeluaranDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PengeluaranDetailPageRoutingModule {}
