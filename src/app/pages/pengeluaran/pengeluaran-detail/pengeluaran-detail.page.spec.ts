import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengeluaranDetailPage } from './pengeluaran-detail.page';

describe('PengeluaranDetailPage', () => {
  let component: PengeluaranDetailPage;
  let fixture: ComponentFixture<PengeluaranDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengeluaranDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengeluaranDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
