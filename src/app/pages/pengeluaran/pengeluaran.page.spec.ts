import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengeluaranPage } from './pengeluaran.page';

describe('PengeluaranPage', () => {
  let component: PengeluaranPage;
  let fixture: ComponentFixture<PengeluaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengeluaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengeluaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
