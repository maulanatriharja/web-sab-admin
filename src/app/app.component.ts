import { Component, OnInit } from '@angular/core';

import { MenuController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Subject } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  isLoggedIn: boolean;

  private login = new Subject<any>();
  pubLogin(data: any) { this.login.next(data); }
  obvLogin(): Subject<any> { return this.login; }

  private transaksi = new Subject<any>();
  pubTransaksi(data: any) { this.transaksi.next(data); }
  obvTransaksi(): Subject<any> { return this.transaksi; }

  private pelanggan = new Subject<any>();
  pubPelanggan(data: any) { this.pelanggan.next(data); }
  obvPelanggan(): Subject<any> { return this.pelanggan; }

  private produk = new Subject<any>();
  pubProduk(data: any) { this.produk.next(data); }
  obvProduk(): Subject<any> { return this.produk; }

  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Penjualan',
      url: '/transaksi',
      icon: 'cart'
    },
    {
      title: 'Pengeluaran',
      url: '/pengeluaran',
      icon: 'arrow-redo-circle'
    },
    {
      title: 'Pelanggan',
      url: '/pelanggan',
      icon: 'person'
    },
    {
      title: 'Produk',
      url: '/produk',
      icon: 'water'
    },
  ];


  constructor(
    public menu: MenuController,
    public platform: Platform,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      //---
      this.obvLogin().subscribe(() => {
        this.isLoggedIn = true;
      });

      const path = window.location.pathname.split('/')[1];
      if (path == '' || path == 'login') {
        this.isLoggedIn = false;
        this.menu.enable(false);
      } else {
        this.isLoggedIn = true;
        this.menu.enable(true);
      }
    });
  }

  ngOnInit() {


    // const path = window.location.pathname.split('folder/')[1];
    // if (path !== undefined) {
    //   this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    // }
  }
}
